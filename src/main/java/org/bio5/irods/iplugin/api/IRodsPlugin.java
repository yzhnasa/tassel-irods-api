/**
 * @author Zhong Yang
 * @author Sharan Babu K
 * @version TASSEL-iRods V1.0
 * */
package org.bio5.irods.iplugin.api;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;
import org.bio5.irods.iplugin.bean.IPlugin;
import org.bio5.irods.iplugin.bean.TasselIrodsCoreFunctions;
import org.bio5.irods.iplugin.connection.IrodsConnection;
import org.bio5.irods.iplugin.fileoperations.FileOperations;
import org.bio5.irods.iplugin.views.DirectoryContentsWindow;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.connection.IRODSSession;
import org.irods.jargon.core.exception.AuthenticationException;
import org.irods.jargon.core.exception.CatalogSQLException;
import org.irods.jargon.core.exception.InvalidUserException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.IRODSFileSystemAOImpl;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.query.CollectionAndDataObjectListingEntry;
/**
 * IRodsPlugin class is the API class which for encapsulate the TASSEL-iRods business logic<br>
 * developer can use it generate the iRods plugin main windows to let user interact with <br>
 * iRods server (file management such as uploading, downloading, etc. ). For using this API <br>
 * developer should include org.bio5.irods.iplugin.api package.
 */
public class IRodsPlugin extends JFrame{
	private String username = null;
	private String password = null;
	private int port;
	private String host = null;
	private String zone = null;
	private boolean isHomeDirectoryTheRootNode;
	private static IPlugin iplugin;
	private IRODSAccount irodsAccount = null;
	private IRODSFileFactory iRODSFileFactory = null;
	static Logger log = Logger.getLogger(IRodsPlugin.class.getName());
	/**
	 * irodsFileSysem is the an instance of file system in iRods server, when developer using<br>
	 * IRodsPlugin class's constructor it will auto instanced and initialed for developer. 
	 * */
	public IRODSFileSystem irodsFileSystem;
	private IRODSFileSystemAOImpl iRODSFileSystemAOImpl;
	private DirectoryContentsWindow directoryContentsPane;
	private JMenuBar menuBar;
	/**
	 * Developer can use this constructor to make an instance of iRods plugin, but when you using this<br>
	 * constructor don't forget set other other attribute to it, such as username, password, port, etc.<br>
	 * if possible we don't recommend use this constructor.
	 * */
	public IRodsPlugin(){
		iplugin = new IPlugin();
		initIrodsFileSystem();
	}
	/**
	 * Developer can use this constructor to make an instance of iRods plugin, this constructor can set <br>
	 * all constructor one time we recommend use this constructor.
	 * @param username iRods username
	 * @param password iRods password
	 * @param port Please set to 1247
	 * @param host Please set to data.iplantcollaborative.org
	 * @param zone Please set to iplant
	 * @param isHodeDirectoryTheRootNode We recommend set false, because if use home director will take <br>
	 * very long time to wait (it need load every user from iRods server to show them in your file tree).
	 * @exception ConnectException Connection timed out: connect
	 * @exception SocketTimeoutException Data server timeout exception. Please restart application!
	 * @exception MalformedURLException Malformed URL Exception!
	 * @exception JargonException Error while creating irodsFileFactory or Error while closing application<br>
	 * or Error while retrieving irodsFileSystem!
	 * @exception CatalogSQLException Invalid Username or password!
	 * @exception InvalidUserException Invalid Username!
	 * @exception AuthenticationException Invalid password!
	 * @exception Exception Unknown Error!
	 * Developer can catch those exception to make dialog window to let user know what's wrong with it. the<br>
	 * using examlpe please see the login method line 578-620 in MainWindow class org.bio5.irods.iplugin.views<br>
	 * package.
	 * IRodsPlugin using example please see testAPI class in org.bio5.irods.iplugin.test package.
	 * */
	public IRodsPlugin(String username, String password, int port, String host, String zone, boolean isHomeDirectoryTheRootNode) 
			throws ConnectException, SocketTimeoutException, MalformedURLException, JargonException, CatalogSQLException, 
			InvalidUserException, AuthenticationException, Exception{
		this.username = username;
		this.password = password;
		this.port = port;
		this.host = host;
		this.zone = zone;
		this.isHomeDirectoryTheRootNode = isHomeDirectoryTheRootNode;
		iplugin = new IPlugin();
		initIrodsFileSystem();
		if(isHomeDirectoryTheRootNode){
			this.iplugin.setHomeDirectoryTheRootNode(true);
		}
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		iplugin.setIRodsPluginMainFrame(this);
		initMainFrame();
		loginMethod();
	}
	/**
	 * Set iRods username, for IRodsPlugin with no parameters, second using style.
	 * @param username iRods username
	 * */
	public void setUsername(String username){
		this.username = username;
	}
	/**
	 * Get iRods username
	 * @return String iRods username
	 * */
	public String getUsername(){
		return username;
	}
	/**
	 * Set iRods password, for IRodsPlugin with no parameters, second using style.
	 * @param password iRods password
	 * */
	public void setPassword(String password){
		this.password = password;
	}
	/**
	 * Get iRods password
	 * @return String iRods password
	 * */
	public String getPassword(){
		return password;
	}
	/**
	 * Set port, for IRodsPlugin with no parameters, second using style.
	 * @param port Please set to 1247
	 * */
	public void setPort(int port){
		this.port = port;
	}
	/**
	 * Get port
	 * @return int Port number
	 * */
	public int getPort(){
		return port;
	}
	/**
	 * Set Host, for IRodsPlugin with no parameters, second using style.
	 * @param host Please set to data.iplantcollaborative.org
	 * */
	public void setHost(String host){
		this.host = host;
	}
	/**
	 * Get Host
	 * @return String Host
	 * */
	public String getHost(){
		return host;
	}
	/**
	 * Set Zone, for IRodsPlugin with no parameters, second using style.
	 * @param zone Please set to iplant
	 * */
	public void setZone(String zone){
		this.zone = zone;
	}
	/**
	 * Get Zone
	 * @return String Zone name
	 * */
	public String getZone(){
		return zone;
	}
	/**
	 * Set home directory as the root node, for IRodsPlugin with no parameters, second using style.
	 * */
	public void setHomeDirectoryTheRootNodeTrue(){
		isHomeDirectoryTheRootNode = true;
		this.iplugin.setHomeDirectoryTheRootNode(true);
	}
	/**
	 * Not set home directory as the root node, for IRodsPlugin with no parameters, second using style.
	 * */
	public void setHomeDirectoryTheRootNodeFalse(){
		isHomeDirectoryTheRootNode = false;
		this.iplugin.setHomeDirectoryTheRootNode(false);
	}
	/**
	 * Make user whether home directory is root node
	 * @return boolean true or false
	 * */
	public boolean isHomeDirectoryTheRootNode(){
		return isHomeDirectoryTheRootNode;
	}
	/**
	 * IPlugin is a bean which use to simplify the iRods plugin design, developer can use it interact with iRods<br>
	 * plugin and set some attribute to it.
	 * @return IPlugin return bean for setting
	 * */
	public IPlugin getIPlugin(){
		return iplugin;
	}
	/**
	 * This login method is for IRodsPlugin with no parameters, first using style.
	 * @param username iRods username
	 * @param password iRods password
	 * @param port Please set to 1247
	 * @param host Please set to data.iplantcollaborative.org
	 * @param zone Please set to iplant
	 * @param isHodeDirectoryTheRootNode We recommend set false, because if use home director will take <br>
	 * very long time to wait (it need load every user from iRods server to show them in your file tree).
	 * @exception ConnectException Connection timed out: connect
	 * @exception SocketTimeoutException Data server timeout exception. Please restart application!
	 * @exception MalformedURLException Malformed URL Exception!
	 * @exception JargonException Error while creating irodsFileFactory or Error while closing application<br>
	 * or Error while retrieving irodsFileSystem!
	 * @exception CatalogSQLException Invalid Username or password!
	 * @exception InvalidUserException Invalid Username!
	 * @exception AuthenticationException Invalid password!
	 * @exception Exception Unknown Error!
	 * */
	public void accountLogin(String username, String password, int port, String host, String zone, boolean isHomeDirectoryTheRootNode) 
			throws ConnectException, SocketTimeoutException, MalformedURLException, JargonException, CatalogSQLException, 
			InvalidUserException, AuthenticationException, Exception{
		this.isHomeDirectoryTheRootNode = isHomeDirectoryTheRootNode;
		if(isHomeDirectoryTheRootNode){
			this.iplugin.setHomeDirectoryTheRootNode(true);
		}
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		iplugin.setIRodsPluginMainFrame(this);
		loginMethod();
	}
	
	private void irodsFileFactoryCreation() {
		try {
			this.iRODSFileFactory = TasselIrodsCoreFunctions
					.getIrodsAccountFileFactory(this.iplugin);

			this.iplugin.setiRODSFileFactory(this.iRODSFileFactory);
		} catch (JargonException jargonException) {
			log.error("Error while creating irodsFileFactory"
					+ jargonException.getMessage());
		}
	}

	private void initIrodsFileSystem() {
		try {
			this.irodsFileSystem = IRODSFileSystem.instance();
			this.iplugin.setIrodsFileSystem(this.irodsFileSystem);
		} catch (JargonException e) {
			log.error("Error while retrieving irodsFileSystem" + e.getMessage());
		}
	}

	private void setVisibilityOfForm() {
		setContentPane(this.directoryContentsPane);
		this.directoryContentsPane.setPreferredSize(getPreferredSize());
		this.directoryContentsPane.setMinimumSize(getMinimumSize());
		validate();
		repaint();
		pack();
		setVisible(true);
	}
	/**
	 * Get minimum size
	 * @return iRods plugin size
	 * */
	public Dimension getMinimumSize() {
		return new Dimension(200, 100);
	}
	/**
	 * Get preferred size
	 * @return iRods plugin size
	 * */
	public Dimension getPreferredSize() {
		return new Dimension(800, 600);
	}
	/**
	 * This login Method, for IRodsPlugin with no parameters, second using style.
	 * @throws ConnectException Connection timed out: connect
	 * @throws SocketTimeoutException Data server timeout exception. Please restart application!
	 * @throws MalformedURLException Malformed URL Exception!
	 * @throws JargonException Error while creating irodsFileFactory or Error while closing application<br>
	 * or Error while retrieving irodsFileSystem!
	 * @throws CatalogSQLException Invalid Username or password!
	 * @throws InvalidUserException Invalid Username!
	 * @throws AuthenticationException Invalid password!
	 * @throws Exception Unknown Error!
	 * */
	public void loginMethod() throws JargonException, ConnectException, SocketTimeoutException, MalformedURLException, 
	CatalogSQLException, InvalidUserException, AuthenticationException, Exception{
		irodsAccount = IrodsConnection.irodsConnection(
				username, password, zone, host, port);
		iplugin.setIrodsAccount(irodsAccount);
		if(iplugin.getIrodsAccount() != null){
			irodsFileFactoryCreation();
		}
		
		if (null != this.irodsFileSystem) {
			IRODSSession iRODSSession = this.irodsFileSystem
					.getIrodsSession();

			this.iplugin.setiRODSSession(iRODSSession);
		} else {
			log.error("iRODSSession is null");
		}
		
		if ((this.iplugin.getIrodsAccount() != null)
				&& (this.iplugin.getiRODSSession() != null)){
			this.iRODSFileSystemAOImpl = new IRODSFileSystemAOImpl(
					this.iplugin.getiRODSSession(),
					this.iplugin.getIrodsAccount());
			if (null != this.iRODSFileSystemAOImpl) {
				this.iplugin
						.setiRODSFileSystemAOImpl(this.iRODSFileSystemAOImpl);
			} else {
				log.error("iRODSFileSystemAOImpl is null");
			}
		}
		List<CollectionAndDataObjectListingEntry> collectionsUnderGivenAbsolutePath = FileOperations
				.setIrodsFile(null, this.iplugin,
						this.iplugin.isHomeDirectoryTheRootNode());
		iplugin.setCollectionsUnderGivenAbsolutePath(collectionsUnderGivenAbsolutePath);
		//initMainFrame();
		this.directoryContentsPane = new DirectoryContentsWindow(
				this.iplugin);
		this.iplugin.setDirectoryContentsPane(this.directoryContentsPane);
		this.directoryContentsPane.init();
		this.directoryContentsPane.implementation();
		setVisibilityOfForm();
		show();
	}
	
	private void initMainFrame(){
		setTitle("TASSEL-iRods");
		setDefaultCloseOperation(2);
		setBounds(100, 100, 682, 454);

		JMenu mnNewMenu_File = new JMenu("File");
		mnNewMenu_File.setMnemonic('F');
		menuBar.add(mnNewMenu_File);

		JMenuItem mntmNewMenuItem_Open = new JMenuItem("Open");
		mntmNewMenuItem_Open.setAccelerator(KeyStroke.getKeyStroke(79, 2));

		mnNewMenu_File.add(mntmNewMenuItem_Open);

		JMenuItem mntmNewMenuItem_Exit = new JMenuItem("Exit");
		mntmNewMenuItem_Exit.setAccelerator(KeyStroke.getKeyStroke(115, 8));

		mntmNewMenuItem_Exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		mnNewMenu_File.add(mntmNewMenuItem_Exit);
		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('H');
		menuBar.add(mnHelp);
		JMenuItem mntm_About = new JMenuItem("About iPlugin");
		mntm_About.setAccelerator(KeyStroke.getKeyStroke(122, 0));
		mntm_About.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane
						.showMessageDialog(
								null,
								"TASSEL-iRods V1.0 - Plugin for TASSEL to handle file operations with iRODS Data Servers",
								"About TASSEL-iRods V1.0", 1);
			}
		});
		mnHelp.add(mntm_About);
	}
	
}
